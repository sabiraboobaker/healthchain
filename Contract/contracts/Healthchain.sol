//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.5.16;

contract Healthchain {
    
    //===============================DECLARATION=============================//
    //**************************user data structure**************************//
    address payable public admin;
    
    struct Doctor{
        bytes name;
        bytes qualification;
        uint experience;
        bytes deptartment;
        address payable pubAdr;
        bytes32 password;
    }
    mapping(bytes => Doctor) public Doctors;
    uint32 public DoctorCount;
    mapping(uint32 => bytes) public DoctorIndex;

    
    struct Patient{
        bytes name;
        bytes dob;
        bytes gender;
        bytes addr;
        bytes bg;
        address payable pubAdr;
        bytes32 password;
        bytes allergies;
    }
    mapping(bytes => Patient) public Patients;
    uint32 public PatientCount;
    mapping(uint32 => bytes) public PatientIndex;
    
    
    //***************************dept********************************//
    mapping(uint => bytes) public Department;
    uint32 public DepartmentCount;
    
    
    //**************************Appointment******************************//
    struct Appointment{
        bytes date;
        bytes doctor;
        bytes patient;
        bool consulted;
        bool history;
        bytes prescription;
        bytes report;
    }
    mapping(uint => Appointment) public Appointments;
    uint public tokenCount;
    
    
    
    //===============================FUNCTIONS=============================//
    //******************************Constructor****************************//
    constructor() public {
        admin=msg.sender;
    }
    
    //****************************User Registration************************//
    //regdoc
    function registerDoctor(bytes memory username, bytes memory name,bytes memory qualification, uint experience,bytes memory department, bytes memory password,address payable pubaddr) public {
        bytes32 passHash = keccak256(password);
        Doctors[username].name = name;
        Doctors[username].qualification = qualification;
        Doctors[username].experience =experience;
        Doctors[username].deptartment =department;
        Doctors[username].pubAdr = pubaddr;
        Doctors[username].password = passHash;
        
        DoctorCount=DoctorCount+1;
        DoctorIndex[DoctorCount]=username;
    }
    
    //regpat
    function registerPatient(bytes memory username, bytes memory name,bytes memory dob, bytes memory gender, bytes memory addr, bytes memory bg, bytes memory allergies, bytes memory password) public {
        bytes32 passHash = keccak256(password);
        Patients[username].name = name;
        Patients[username].dob = dob;
        Patients[username].gender =gender;
        Patients[username].addr =addr;
        Patients[username].bg =bg;
        Patients[username].allergies =allergies;
        Patients[username].pubAdr = msg.sender;
        Patients[username].password = passHash;
        
        PatientCount=PatientCount+1;
        PatientIndex[PatientCount]=username;
    }
    
    
    
    //****************************login***********************************//
    function login(bytes memory username, bytes memory password) public view returns (bool,uint,bytes memory,bytes memory,address) {
        bytes32 passHash = keccak256(password);
        if (Doctors[username].password == passHash) {
            return (true,2,username,Doctors[username].name,Doctors[username].pubAdr);
        }else if (Patients[username].password == passHash) {
            return (true,1,username,Patients[username].name,Patients[username].pubAdr);
        }else {
            return (false,0,bytes(""),bytes(""),address(0));
        }
    }
    
    
    
    //*************************User Details*******************************//
    //docdet
    function getDoctor(bytes memory username) public view returns( bytes memory name,bytes memory qualification, uint experience,bytes memory department,address payable pubaddr){
        return(
            Doctors[username].name,
            Doctors[username].qualification,
            Doctors[username].experience,
            Doctors[username].deptartment,
            Doctors[username].pubAdr
        );
    }
    
    //patdet
    function getPatient(bytes memory username) public view returns( bytes memory name,bytes memory dob, bytes memory gender,bytes memory addr,bytes memory bg, bytes memory allergies,address payable pubaddr){
        return(
            Patients[username].name,
            Patients[username].dob,
            Patients[username].gender,
            Patients[username].addr,
            Patients[username].bg,
            Patients[username].allergies,
            Patients[username].pubAdr
        );
    }
    
    
    
    //**********************Department*************************//             
    
    function addDept(bytes memory dept) public{
        DepartmentCount=DepartmentCount+1;
        Department[DepartmentCount]=dept;
    }
    
    function getDept(uint index) public view returns(bytes memory dept){
        return  Department[index];
    }
    
    
    //********************Appointment****************************//
    function bookAppointment(bytes memory date,bytes memory doctor,bytes memory patient,bool history) public payable{
        if(msg.value!=5000000000000000000){
            revert("Booking payment should be 5 Ether");
        }
        tokenCount = tokenCount +1;
        Appointments[tokenCount].date = date;
        Appointments[tokenCount].doctor = doctor;
        Appointments[tokenCount].patient = patient;
        Appointments[tokenCount].consulted = false;
        Appointments[tokenCount].history = history;
        Appointments[tokenCount].prescription = bytes("");
        Appointments[tokenCount].report = bytes("");
    }
    
    //*********************add prescription and report******************//
    function consultaionUpdate(uint token, bytes memory prescription, bytes memory report)public{
        Appointments[token].prescription = prescription;
        Appointments[token].report = report;
        Appointments[token].consulted = true;

    }
}
