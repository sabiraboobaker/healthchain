var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/*-------------------WEB3 Connection Starts---------------------*/
const Web3 = require('web3');
var HealthchainJSON = require(path.join(__dirname, './../Contract/build/contracts/Healthchain.json'));

//for ganache
web3 = new Web3('ws://localhost:7545');
HealthchainAddress = HealthchainJSON.networks['5777'].address;

const HealthchainAbi = HealthchainJSON.abi;

Healthchain = new web3.eth.Contract(HealthchainAbi, HealthchainAddress);
/*-------------------WEB3 Connection Ends----------------------*/

var auth = require('./routes/auth');
var admin = require('./routes/admin');
var doctor = require('./routes/doctor');
var patient = require('./routes/patient');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Cache-control,Pragma,Origin,Authorization,Content-Type,X-Requested-With");
    res.header("Access-Control-Allow-Methods","GET,PUT,POST");
    if('OPTIONS'===req.method){
        res.status(204).send();
    }
    else{
        next();
    }
});

app.use('/', auth);
app.use('/admin', admin);
app.use('/doctor', doctor);
app.use('/patient', patient);

module.exports = app;
