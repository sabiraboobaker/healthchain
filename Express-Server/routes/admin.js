var express = require('express');
var router = express.Router();

router.post('/new-department', async function (req, res, next) {
    data = req.body;
    let dept = web3.utils.asciiToHex(data.dept);
    let publicAddress = data.publicAddress;

    console.log("new dept called")

    var resp = await Healthchain.methods.addDept(
        dept
    ).send({ from: publicAddress });

    console.log(resp);

    res.status(200).json({ status: true });
});

router.post('/list-department', async function (req, res, next) {

    console.log("new dept called")

    var resp1 = await Healthchain.methods.DepartmentCount().call();
    console.log(resp1)
    var deptList = []
    for (var i = 1; i <= resp1; i++) {
        var resp2 = await Healthchain.methods.Department(i).call();
        console.log(resp2)
        deptList.push(web3.utils.hexToAscii(resp2));
    }

    console.log(deptList);

    res.status(200).json({ list: deptList });
});

router.post('/new-doctor', async function (req, res, next) {
    data = req.body;
    let name = web3.utils.asciiToHex(data.name);
    let username = web3.utils.asciiToHex(data.username);
    let qualification = web3.utils.asciiToHex(data.qualification);
    let experience = web3.utils.asciiToHex(data.experience);
    let department = web3.utils.asciiToHex(data.department);
    let publicAddress = data.publicAddress;
    let password = web3.utils.asciiToHex(data.password);
    console.log("register called")

    var resp = await Healthchain.methods.registerDoctor(
        username,
        name,
        qualification,
        experience,
        department,
        password,
        publicAddress
    ).send({ from: publicAddress, gas: 6000000 });

    console.log(resp);

    res.status(200).json({ status: true });
});


router.post('/list-doctor', async function (req, res, next) {

    console.log("new dept called")

    var resp1 = await Healthchain.methods.DoctorCount().call();
    console.log(resp1)
    var docList = []
    for (var i = 1; i <= resp1; i++) {
        var resp2 = await Healthchain.methods.DoctorIndex(i).call();
        var resp3 = await Healthchain.methods.Doctors(resp2).call();
        console.log(resp3)
        docList.push({"username":web3.utils.hexToAscii(resp2),"name":web3.utils.hexToAscii(resp3[0]),"department":web3.utils.hexToAscii(resp3[3]),"qualification":web3.utils.hexToAscii(resp3[1]), "experience":resp3[2]});
    }

    console.log(docList);

    res.status(200).json({ list: docList });
});

router.post('/list-patient', async function (req, res, next) {

    console.log("new dept called")

    var resp1 = await Healthchain.methods.PatientCount().call();
    console.log(resp1)
    var patList = []
    for (var i = 1; i <= resp1; i++) {
        var resp2 = await Healthchain.methods.PatientIndex(i).call();
        var resp3 = await Healthchain.methods.Patients(resp2).call();
        console.log(resp3)
        patList.push({"username":web3.utils.hexToAscii(resp2),"name":web3.utils.hexToAscii(resp3[0]),"dob":web3.utils.hexToAscii(resp3[1]),"gender":web3.utils.hexToAscii(resp3[2])});
    }

    console.log(patList);

    res.status(200).json({ list: patList });
});

router.post('/list-appointment', async function (req, res, next) {

    console.log("new dept called")

    var resp1 = await Healthchain.methods.tokenCount().call();
    console.log(resp1)
    var aptList = []
    for (var i = 1; i <= resp1; i++) {
        var resp2 = await Healthchain.methods.Appointments(i).call();
        console.log(resp2)
        aptList.push({"token":i,"date":web3.utils.hexToAscii(resp2[0]),"doctor":web3.utils.hexToAscii(resp2[1]),"patient":web3.utils.hexToAscii(resp2[2]),"prescription":web3.utils.hexToAscii(resp2[5]),"report":web3.utils.hexToAscii(resp2[6])});
    }

    console.log(aptList);

    res.status(200).json({ list: aptList });
});

module.exports = router;
