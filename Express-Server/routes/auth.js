var express = require('express');
var router = express.Router();

router.post('/login', async function (req, res, next) {
    let data = req.body;
    console.log(data)
  
    let username = web3.utils.asciiToHex(data.username);
    let password = web3.utils.asciiToHex(data.password);
    console.log(password)
    if(username=="0x61646d696e" && password=="0x61646d696e"){
      var accounts = await web3.eth.personal.getAccounts();
      res.status(200).json({ status: true,type:"3" ,username:"admin",name:"Admin", pubAddr:accounts[0]});
      return;
    }
    Healthchain.methods.login(username,password)
      .call()
      .then((val) => {
        console.log("heyyy",val);
        if (val[0] == true) {
          res.status(200).json({ status: true,type:val[1],username:web3.utils.hexToAscii(val[2]),name:web3.utils.hexToAscii(val[3]), pubAddr:val[4] });
        } else {
          res.status(400).json({ status: false });
        }
      })
});

router.post('/register', async function (req, res, next) {
    data = req.body;
    let name = web3.utils.asciiToHex(data.name);
    let username = web3.utils.asciiToHex(data.username);
    let gender = web3.utils.asciiToHex(data.gender);
    let allergies = web3.utils.asciiToHex(data.allergies);
    let address = web3.utils.asciiToHex(data.address);
    let bg = web3.utils.asciiToHex(data.bg);
    let dob = web3.utils.asciiToHex(data.dob);
    let publicAddress = data.publicAddress;
    let password = web3.utils.asciiToHex(data.password);
    console.log("register called")

    var resp = await Healthchain.methods.registerPatient(
        username,
        name,
        dob,
        gender,
        address,
        bg,
        allergies,
        password
    ).send({ from: publicAddress,gas:6000000 });

    console.log(resp);

    res.status(200).json({ status: true });
    // signTxn.sendTransaction(CrowdFundingAddress, methodCall, publicAddress, privateKey, null, function (
    //     response
    // ) {
    //     if (response == true) res.status(200).json({ status: true });
    //     else res.status(400).json({ status: false });
    // });
});

module.exports = router;
