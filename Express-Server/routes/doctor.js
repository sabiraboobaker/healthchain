var express = require('express');
var router = express.Router();

router.post('/view-appointment', async function(req, res, next) {
    data = req.body;
    let token = data.token;
    var resp = await Healthchain.methods.Appointments(token).call();
    console.log(resp);
    var pastList=[]
    if(resp[4]==true){
        console.log("history is true")
        for (var i = 1; i < token; i++) {
            var resp2 = await Healthchain.methods.Appointments(i).call();
            if(resp2[2]==resp[2]){
                console.log(resp2)
                if(resp2[3]==true)
                    pastList.push({date:web3.utils.hexToAscii(resp2[0]),doctor:web3.utils.hexToAscii(resp2[1]),prescription:web3.utils.hexToAscii(resp2[5]),report:web3.utils.hexToAscii(resp2[6])})
            
            }
        }
    }

    res.status(200).json({date:web3.utils.hexToAscii(resp[0]),patient:web3.utils.hexToAscii(resp[2]),history:pastList});
});

router.post('/add-report', async function(req, res, next) {
    data = req.body;
    let token = data.token;
    let report = web3.utils.asciiToHex(data.report);
    let prescription = web3.utils.asciiToHex(data.prescription);
    let publicAddress = data.publicAddress;
    var resp = await Healthchain.methods.consultaionUpdate(
        token,
        prescription,
        report
    ).send({ from: publicAddress, gas: 6000000 });
    console.log(resp);

    res.status(200).json({ status: true });
});

router.post('/list-appointment', async function (req, res, next) {
    var data = req.body
    console.log("new dept called")
    let username = web3.utils.asciiToHex(data.username);

    var resp1 = await Healthchain.methods.tokenCount().call();
    console.log(resp1)
    var aptList = []
    var pastList=[]
    for (var i = 1; i <= resp1; i++) {
        var resp2 = await Healthchain.methods.Appointments(i).call();
        if(resp2[1]==username){
            console.log(resp2)
            if(resp2[3]==true)
                pastList.push({date:web3.utils.hexToAscii(resp2[0]),patient:web3.utils.hexToAscii(resp2[2]),prescription:web3.utils.hexToAscii(resp2[5]),report:web3.utils.hexToAscii(resp2[6])})
            else
                aptList.push({"token":i,"date":web3.utils.hexToAscii(resp2[0]),"patient":web3.utils.hexToAscii(resp2[2])});
        }
    }

    console.log(aptList);
    console.log(pastList);

    res.status(200).json({  aptList,pastList });
});


module.exports = router;
