var express = require('express');
var router = express.Router();

router.post('/new-appointment', async function(req, res, next) {
    data = req.body;
    let username = web3.utils.asciiToHex(data.username);
    let doctor = web3.utils.asciiToHex(data.doctor);
    let date = web3.utils.asciiToHex(data.date);
    let history = data.history;
    let publicAddress = data.publicAddress;
    var resp = await Healthchain.methods.bookAppointment(
        date,
        doctor,
        username,
        history
    ).send({ from: publicAddress, gas: 6000000, value:web3.utils.toWei(String(5), 'ether') });
    console.log(resp);

    res.status(200).json({ status: true });
});

router.post('/list-appointment', async function (req, res, next) {
    var data = req.body
    console.log("new dept called")
    let username = web3.utils.asciiToHex(data.username);

    var resp1 = await Healthchain.methods.tokenCount().call();
    console.log(resp1)
    var aptList = []
    var pastList=[]
    for (var i = 1; i <= resp1; i++) {
        var resp2 = await Healthchain.methods.Appointments(i).call();
        if(resp2[2]==username){
            console.log(resp2)
            if(resp2[3]==true)
                pastList.push({date:web3.utils.hexToAscii(resp2[0]),doctor:web3.utils.hexToAscii(resp2[1]),prescription:web3.utils.hexToAscii(resp2[5]),report:web3.utils.hexToAscii(resp2[6])})
            else
                aptList.push({"token":i,"date":web3.utils.hexToAscii(resp2[0]),"doctor":web3.utils.hexToAscii(resp2[1]),"patient":web3.utils.hexToAscii(resp2[2])});
        }
    }

    console.log(aptList);
    console.log(pastList);

    res.status(200).json({  aptList,pastList });
});


module.exports = router;
