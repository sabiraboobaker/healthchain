import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Login from './Pages/Login'
import Register from './Pages/Register'
import Admin from './Pages/Admin'
import Doctor from './Pages/Doctor'
import Patient from './Pages/Patient'
import './App.css'
function App() {
  return (
    <BrowserRouter>
      <Switch>
        {/* <Route exact path={"/login"} name={"Login Page"} render={props => (sessionStorage.getItem("isLoggedin") === "false" ? < Home {...props} /> : <Redirect to="/" />)} /> */}
        <Route path={"/admin"} name={"Admin"} render={props =><Admin {...props} />} />
        <Route path={"/doctor"} name={"Doctor"} render={props =><Doctor {...props} />} />
        <Route path={"/patient"} name={"Patient"} render={props =><Patient {...props} />} />
        <Route path={"/register"} name={"Register"} render={props =><Register {...props} />} />
        <Route path={"/"} name={"Login"} render={props =><Login {...props} />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
