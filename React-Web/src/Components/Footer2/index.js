import React, { Component } from 'react';
import './footer.css';

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer className="footer-distributed">
                <div className="footer-left">
                    <h3>HealthChain</h3>
                </div>

                <div className="footer-center">
                    <p className="footer-links">

                    </p>
                </div>

                <div className="footer-right">
                    <p className="footer-company-name">HealthChain &copy; 2021</p>
                </div>
            </footer>
        )
    }
}
export default Footer;