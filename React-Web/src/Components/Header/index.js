import React, { Component } from 'react';


class Header extends Component {
    constructor(props) {
        super(props);
    }

    logout(){
        window.sessionStorage.clear()
        window.location.href="/"
    }

    render() {
        return (
            <nav>
                <div className="nav-wrapper indigo">
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li><a href="#" onClick={()=>this.logout()}>Logout</a></li>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default Header;