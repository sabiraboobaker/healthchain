import React, { Component } from 'react';


class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav>
                <div className="nav-wrapper indigo">
                    <a href="#" className="brand-logo">HealthChain</a>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        {/* <li><a href="/login">Logout</a></li> */}
                    </ul>
                </div>
            </nav>
        )
    }
}
export default Header;