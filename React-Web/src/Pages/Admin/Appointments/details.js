import React, { Component } from 'react';


class AppointmentDetail extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row" style={{height: "76vh"}}>
                <div className="col s6 offset-s3">
                    <h4>Appointment Details</h4>
                </div>
                <div className="col s6 offset-s3">
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate"  value="12/05/2021"/>
                            <label for="name">Date</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate"  value="Rahul"/>
                            <label for="name">Patient Name</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate"  value="Cardiology"/>
                            <label for="name">Department</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate"  value="Dr Shyam"/>
                            <label for="name">Doctor Name</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate"  value="1"/>
                            <label for="name">Token</label>
                        </div>
                    </div>

                    {/* <div className="row">
                        <div className="input-field col s12">
                            <a className="btn waves-effect waves-light" name="login">ADD
                    <i className="material-icons left">add</i>
                            </a>
                        </div>
                    </div> */}
                </div>
            </div>


        )
    }
}
export default AppointmentDetail;