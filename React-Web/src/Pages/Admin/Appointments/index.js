import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class AppointmentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            aptList: []
        }
    }

    async componentDidMount() {
        var payload = {

        }
        let apiResponse = await apiCall(
            "admin/list-appointment",
            payload
        );
        console.log(apiResponse)
        this.setState({ aptList: apiResponse["list"] })
    }

    render() {
        return (
            <div className="row"  style={{ height: "76vh" }}>
                <div className="col s12">
                    <h4>Appointment List</h4>
                </div>
                <div className="col s12">
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>Token</th>
                                <th>Date</th>
                                <th>Patient Name</th>
                                <th>Doctor</th>
                                <th>Prescription</th>
                                <th>Report</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                                this.state.aptList.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{item["token"]}</td>
                                            <td>{item["date"]}</td>
                                            <td>{item["patient"]}</td>
                                            <td>{item["doctor"]}</td>
                                            <td>{item["prescription"]?item["prescription"]:"No Prescription"}</td>
                                            <td>{item["report"]?item["report"]:"No Report"}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
export default AppointmentsList;