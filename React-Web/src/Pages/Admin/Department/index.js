import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class DepartmentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deptList: []
        }
    }

    async componentDidMount() {
        var payload = {

        }
        let apiResponse = await apiCall(
            "admin/list-department",
            payload
        );
        console.log(apiResponse)
        this.setState({ deptList: apiResponse["list"] })
    }

    render() {
        return (
            <div className="row" style={{ height: "76vh" }}>
                <div className="col s12">
                    <h4>Department List</h4>
                    <a href="/admin/department-list/new" className="waves-effect waves-light btn" style={{ float: "right" }}><i className="material-icons left">add</i>New
                    department</a>

                </div>
                <div className="col s12">
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Department</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.deptList.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item}</td>
                                        </tr>
                                    )
                                })
                            }


                        </tbody>
                    </table>
                    {/* <label className="center"><h6 className=" white black-text"> No items to show</h6></label> */}
                </div>
            </div>
        )
    }
}
export default DepartmentList;