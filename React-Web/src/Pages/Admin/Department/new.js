import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class DepartmentNew extends Component {
    constructor(props) {
        super(props);
        this.state={
            dept:""
        }
    }

    async AddDept(){
        if (this.state.dept == null || this.state.dept == "") {
            alert("Deptartment can't be blank");
            return false;
        }
        var payload={
            dept:this.state.dept,
            publicAddress:window.sessionStorage.getItem("publicAddress")
        }
        console.log(payload)
        let apiResponse = await apiCall(
            "admin/new-department",
            payload
        );
        console.log(apiResponse)
        window.location.href="/admin/department-list";

    }

    render() {
        return (
            <div className="row" style={{height: "76vh"}}>
            <div className="col s6 offset-s3">
                <h4>New Department</h4>
            </div>
            <div className="col s6 offset-s3">
                <div className="row">
                    <div className="input-field col s12">
                        <input type="text" id="name" className="validate"  value={this.state.dept} onChange={(e)=>{this.setState({dept:e.target.value})}}/>
                        <label for="name">Department Name</label>
                    </div>
                </div>
                

                <div className="row">
                    <div className="input-field col s12">
                        <a className="btn waves-effect waves-light" name="login" onClick={()=>this.AddDept()}>ADD
                <i className="material-icons left">add</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
export default DepartmentNew;