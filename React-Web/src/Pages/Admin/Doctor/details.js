import React, { Component } from 'react';


class DoctorDetails extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row" style={{ height: "76vh" }}>
                <div className="col s6 offset-s3">
                    <h4>Doctor Details</h4>
                </div>
                <div className="col s6 offset-s3">
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="shyam" />
                            <label for="name">Username</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="Dr Shyam" />
                            <label for="name">Name</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="Cardiology" />
                            <label for="name">Department</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="MBBS" />
                            <label for="name">Qualification</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="2 yrs" />
                            <label for="name">Experience</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="0xC30c1d5c5C933A6f71EB1b39cB96Cd983068D66E" />
                            <label for="name">Public Address</label>
                        </div>
                    </div>

                    {/* <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value="1" />
                            <label for="name">Username</label>
                        </div>
                    </div> */}
                    {/* <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate"  value="1"/>
                            <label for="name">Public Address</label>
                        </div>
                    </div> */}

                    {/* <div className="row">
                        <div className="input-field col s12">
                            <a className="btn waves-effect waves-light" name="login">ADD
                    <i className="material-icons left">add</i>
                            </a>
                        </div>
                    </div> */}
                </div>
            </div>
        )
    }
}
export default DoctorDetails;