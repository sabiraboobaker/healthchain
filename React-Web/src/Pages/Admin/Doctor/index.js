import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class DoctorList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            docList: []
        }
    }

    async componentDidMount() {
        var payload = {

        }
        let apiResponse = await apiCall(
            "admin/list-doctor",
            payload
        );
        console.log(apiResponse)
        this.setState({ docList: apiResponse["list"] })
    }

    render() {
        return (
            <div className="row"  style={{height: "76vh"}}>
                <div className="col s12">
                    <h4>Doctor List</h4>
                    <a href="/admin/doctors-list/new" className="waves-effect waves-light btn" style={{ float: "right" }}><i className="material-icons left">add</i>Register Doctor</a>

                </div>
                <div className="col s12">
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Qualification</th>
                                <th>Experience</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                                this.state.docList.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item["username"]}</td>
                                            <td>{item["name"]}</td>
                                            <td>{item["department"]}</td>
                                            <td>{item["qualification"]}</td>
                                            <td>{item["experience"]}</td>
                                        </tr>
                                    )
                                })
                            }

                        </tbody>
                    </table>
                    {/* <label className="center"><h6 className=" white black-text"> No items to show</h6></label> */}
                </div>
            </div>
        )
    }
}
export default DoctorList;