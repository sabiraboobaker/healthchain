import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class DoctorNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deptList: [],
            name: "",
            department: "",
            qualification: "",
            experience: 0,
            pubAddr: "",
            password: "",
            username: "",

        }
    }

    async componentDidMount() {
        var payload = {

        }
        let apiResponse = await apiCall(
            "admin/list-department",
            payload
        );
        console.log(apiResponse)
        this.setState({ deptList: apiResponse["list"] })
    }

    async RegisterDoctor() {
        if (this.state.name == null || this.state.name == "") {
            alert("Name can't be blank");
            return false;
        }else  if (this.state.department == null || this.state.department == "") {
            alert("Please select Department");
            return false;
        }else  if (this.state.qualification == null || this.state.qualification == "") {
            alert("Qualification can't be blank");
            return false;
        }else  if (this.state.experience == null || this.state.experience == 0) {
            alert("Experience can't be blank");
            return false;
        }else  if (this.state.pubAddr == null || this.state.pubAddr == "") {
            alert("Public Address can't be blank");
            return false;
        }else  if (this.state.username == null || this.state.username == "") {
            alert("Username can't be blank");
            return false;
        }else if (this.state.password.length < 5) {
            alert("Password must be at least 6 characters long.");
            return false;
        }
        var payload = {
            name: this.state.name,
            department: this.state.department,
            qualification: this.state.qualification,
            experience: this.state.experience,
            publicAddress: this.state.pubAddr,
            password: this.state.password,
            username: this.state.username,
        }
        let apiResponse = await apiCall(
            "admin/new-doctor",
            payload
        );
        console.log(apiResponse)
        window.location.href = "/admin/doctors-list";
    }

    render() {
        return (
            <div className="row" >
                <div className="col s6 offset-s3">
                    <h4>Doctor Registration</h4>
                </div>
                <div className="col s6 offset-s3">
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="name" className="validate" value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }) }} />
                            <label for="name">Name</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <select style={{ display: "initial" }} value={this.state.department} onChange={(e) => { this.setState({ department: e.target.value }) }}>
                                <option value="" disabled selected>Department</option>
                                {
                                    this.state.deptList?.map((element) => {
                                        return (<option value={element}>{element}</option>);
                                    })
                                }
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="qualification" className="validate" value={this.state.qualification} onChange={(e) => { this.setState({ qualification: e.target.value }) }} />
                            <label for="qualification">Qualification</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="number" id="experience" className="validate" value={this.state.experience} onChange={(e) => { this.setState({ experience: e.target.value }) }} />
                            <label for="experience">Experience</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="pubAddr" className="validate" value={this.state.pubAddr} onChange={(e) => { this.setState({ pubAddr: e.target.value }) }} />
                            <label for="pubAddr">Public Address</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="username" className="validate" value={this.state.username} onChange={(e) => { this.setState({ username: e.target.value }) }} />
                            <label for="username">Username</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="password" id="password" className="validate" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }) }} />
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12">
                            <a className="btn waves-effect waves-light" name="login" onClick={() => this.RegisterDoctor()}>ADD
                    <i className="material-icons left">add</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default DoctorNew;