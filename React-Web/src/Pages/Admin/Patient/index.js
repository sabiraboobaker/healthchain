import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class PatientList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patList: []
        }
    }

    async componentDidMount() {
        var payload = {

        }
        let apiResponse = await apiCall(
            "admin/list-patient",
            payload
        );
        console.log(apiResponse)
        this.setState({ patList: apiResponse["list"] })
    }

    render() {
        return (
            <div className="row"  style={{height: "76vh"}}>
                <div className="col s12">
                    <h4>Patient List</h4>
                </div>
                <div className="col s12">
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>DOB</th>
                                <th>Gender</th>

                            </tr>
                        </thead>
                        <tbody>
                        {
                                this.state.patList.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item["username"]}</td>
                                            <td>{item["name"]}</td>
                                            <td>{item["dob"]}</td>
                                            <td>{item["gender"]}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                    {/* <label className="center"><h6 className=" white black-text"> No items to show</h6></label> */}
                </div>
            </div>
        )
    }
}
export default PatientList;