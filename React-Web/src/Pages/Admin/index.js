import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Header from './../../Components/Header';
import Footer from './../../Components/Footer';
import AppointmentList from './Appointments';
import AppointmentDetail from './Appointments/details';
import PatientList from './Patient';
import DoctorList from './Doctor';
import DoctorNew from './Doctor/new';
import DoctorDetail from './Doctor/details';
import DepartmentList from './Department';
import DepartmentNew from './Department/new';

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state={
            name : window.sessionStorage.getItem("name"),
            pubAddr :window.sessionStorage.getItem("publicAddress"),
        }
    }

    render() {
        return (
            <div>
                <ul id="slide-out" className="sidenav sidenav-fixed" style={{backgroundColor: "#7986cb"}}>
                    <li>
                    <a href="#" className="brand-logo" style={{backgroundColor: "#3f51b5",height: "64px",fontSize: "30px",color: "white"}}>HealthChain</a>
                        <div className="user-view">
                            <div className="background" style={{backgroundColor: "#5c6bc0 "}}>
                            </div>
                            <a ><span className="white-text name"><h5>{this.state.name}</h5></span></a>
                            <a ><span className="white-text email" style={{fontSize: "11px"}}>{this.state.pubAddr}</span></a>
                        </div>
                    </li>
                    <li style={{borderBottom:"1px solid white"}}><a href="/admin" >Appointments</a></li>
                    <li style={{borderBottom:"1px solid white"}}><a href="/admin/patients-list" >Patients</a></li>
                    <li style={{borderBottom:"1px solid white"}}><a href="/admin/department-list" >Departments</a></li>
                    <li style={{borderBottom:"1px solid white"}}><a href="/admin/doctors-list" >Doctors</a></li>

                </ul>
                <div className="wrapper">
                <Header/>
                <Route path={"/admin/"} exact={true} component={AppointmentList} />
                <Route path={"/admin/appointment/detail"} exact={true} component={AppointmentDetail} />
                <Route path={"/admin/patients-list"} exact={true} component={PatientList} />
                <Route path={"/admin/doctors-list"} exact={true} component={DoctorList} />
                <Route path={"/admin/doctors-list/detail"} exact={true} component={DoctorDetail} />
                <Route path={"/admin/doctors-list/new"} exact={true} component={DoctorNew} />
                <Route path={"/admin/department-list"} exact={true} component={DepartmentList} />
                <Route path={"/admin/department-list/new"} exact={true} component={DepartmentNew} />
                <Footer />

                </div>
            </div>
        )
    }
}
export default Admin;