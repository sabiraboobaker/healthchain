import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class AppointmentDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: "",
            name: "",
            token: 0,
            prescription: "",
            report: "",
            history:[]
        }
    }

    async componentDidMount() {
        var payload = {
            token: this.props.match.params["token"]
        }
        let apiResponse = await apiCall(
            "doctor/view-appointment",
            payload
        );
        console.log(apiResponse)
        this.setState({ date: apiResponse["date"], name: apiResponse["patient"], token: this.props.match.params["token"],history:apiResponse["history"] })
    }

    async submitUpdate(){
        if (this.state.report == null || this.state.report == "") {
            alert("Report can't be blank");
            return false;
        }
        var payload = {
            token: this.props.match.params["token"],
            report:this.state.report,
            prescription:this.state.prescription,
            publicAddress:window.sessionStorage.getItem("publicAddress")
        }
        let apiResponse = await apiCall(
            "doctor/add-report",
            payload
        );
        console.log(apiResponse)
        window.location.href="/doctor";
    }

    render() {
        return (
            <div className="row" style={{ height: "76vh" }}>
                <div className="col s6 offset-s3">
                    <h4>Appointment Details</h4>
                </div>
                <div className="col s6 offset-s3">
                    <div className="row">
                        <div className="col s4">
                            <h6 >Token: <b>{this.state.token}</b></h6>
                        </div>
                        <div className="col s4">
                            <h6 >Patient Name: <b>{this.state.name}</b></h6>
                        </div>
                        <div className="col s4">
                            <h6 >Date: <b>{this.state.date}</b></h6>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s6">
                            <textarea type="text" id="prescription" style={{ minHeight: "200px" }} className="validate" value={this.state.prescription} onChange={(e) => { this.setState({ prescription: e.target.value }) }} />
                            <label for="prescription">Prescription</label>
                        </div>
                        <div className="input-field col s6">
                            <textarea type="text" id="report" style={{ minHeight: "200px" }} className="validate" value={this.state.report} onChange={(e) => { this.setState({ report: e.target.value }) }} />
                            <label for="report">Report</label>
                        </div>

                    </div>

                    <div className="row">
                        <div className="input-field col s12">
                            <a className="btn waves-effect waves-light" name="login" onClick={()=>this.submitUpdate()}>Submit
                    <i className="material-icons left">check</i>
                            </a>
                        </div>
                    </div>
</div>
                    <div className="col s6 offset-s3">
                    <h4>Treatment History</h4>
                </div>
                <div className="col s12" style={{marginBottom:"70px"}}>
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Date</th>
                                <th>Patient</th>
                                <th>Prescription</th>
                                <th>Report</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.history.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item["date"]}</td>
                                            <td>{item["doctor"]}</td>
                                            <td>{item["prescription"]}</td>
                                            <td>{item["report"]}</td>
                                        </tr>
                                    )
                                })
                            }

                        </tbody>
                    </table>
                </div>

            </div>


        )
    }
}
export default AppointmentDetail;