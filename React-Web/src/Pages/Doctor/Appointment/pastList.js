import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class AppointmentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            aptList: [],
        }
    }

    async componentDidMount() {
        var payload = {
            username: window.sessionStorage.getItem("username")
        }
        let apiResponse = await apiCall(
            "doctor/list-appointment",
            payload
        );
        console.log(apiResponse)
        this.setState({ aptList: apiResponse["pastList"] })
    }

    render() {
        return (
            <div className="row" style={{ height: "76vh" }}>
                <div className="col s12">
                    <h4>Past Appointments</h4>
                </div>
                <div className="col s12">
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Patient</th>
                                <th>Prescription</th>
                                <th>Report</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.aptList.map((item, index) => {
                                    return (
                                        <tr >
                                            <td>{index+1}</td>
                                            <td>{item["date"]}</td>
                                            <td>{item["patient"]}</td>
                                            <td>{item["prescription"]}</td>
                                            <td>{item["report"]}</td>
                                        </tr>
                                    )
                                })
                            }

                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
export default AppointmentsList;