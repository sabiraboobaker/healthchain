import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Header from './../../Components/Header'
import Footer from './../../Components/Footer'

import AppointmentListNew from './Appointment/newList';
import AppointmentListPast from './Appointment/pastList';
import AppointmentView from './Appointment/detail';


class Doctor extends Component {
    constructor(props) {
        super(props);
        this.state={
            name : window.sessionStorage.getItem("name"),
            pubAddr :window.sessionStorage.getItem("publicAddress"),
        }
    }

    render() {
        return (
            <div>
                <ul id="slide-out" className="sidenav sidenav-fixed" style={{ backgroundColor: "#7986cb" }}>
                    <li>
                        <a href="#" className="brand-logo" style={{ backgroundColor: "#3f51b5", height: "64px", fontSize: "30px", color: "white" }}>HealthChain</a>
                        <div className="user-view">
                            <div className="background" style={{ backgroundColor: "#5c6bc0 " }}>
                            </div>
                            <a><span className="white-text name"><h5>{this.state.name}</h5></span></a>
                            <a><span className="white-text email" style={{ fontSize: "11px" }}>{this.state.pubAddr}</span></a>
                        </div>
                    </li>
                    <li style={{ borderBottom: "1px solid white" }}><a href="/doctor">New Appointments</a></li>
                    <li style={{ borderBottom: "1px solid white" }}><a href="/doctor/appointments-past">Past Appointments</a></li>


                </ul>
                <div className="wrapper">
                    <Header />
                    <Route path={"/doctor"} exact={true} component={AppointmentListNew} />
                    <Route path={"/doctor/appointments-past"} exact={true} component={AppointmentListPast} />
                    <Route path={"/doctor/appointments/:token"} exact={true} component={AppointmentView} />
                    <Footer />
                </div>
            </div>
        )
    }
}
export default Doctor;