import React, { Component } from 'react';
import Header from './../../Components/Header2'
import Footer from './../../Components/Footer2'
import apiCall from './../../Services/apiV1'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        }
    }

    async userLogin() {

        if (this.state.username == null || this.state.username == "") {
            alert("username can't be blank");
            return false;
        } else if (this.state.password.length < 5) {
            alert("Password must be at least 6 characters long.");
            return false;
        }

        var payload = {
            username: this.state.username,
            password: this.state.password
        }
        console.log(payload)
        let apiResponse = await apiCall(
            "login",
            payload
        );
        console.log(apiResponse)
        if (apiResponse["status"] == true) {
            window.sessionStorage.setItem("username", apiResponse["username"]);
            window.sessionStorage.setItem("name", apiResponse["name"]);
            window.sessionStorage.setItem("publicAddress", apiResponse["pubAddr"]);
            if (apiResponse["type"] == "3") {
                window.location.href = "/admin";
            } else if (apiResponse["type"] == "2") {
                window.location.href = "/doctor";
            } else if (apiResponse["type"] == "1") {
                window.location.href = "/patient";
            }
        } else {
            alert("Invalid Credentials")
        }
    }

    render() {
        return (
            <div>
                <Header />
                <div className="valign-wrapper indigo lighten-4" style={{ height: "90vh" }}>
                    <div className="row" style={{ width: "100vw" }}>
                        <div className="col s12 m4 offset-m4">
                            <div className="card horizontal">
                                <div className="card-stacked">
                                    <div className="card-content">
                                        <span className="card-title">Login</span>
                                        <div className="row">
                                            <div className="col s12">
                                                <div className="row">
                                                    <div className="input-field col s12">
                                                        <input type="text" id="username" className="validate" autofocus value={this.state.username} onChange={(e) => { this.setState({ username: e.target.value }) }} />
                                                        <label for="username">Username</label>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="input-field col s12">
                                                        <input id="password" type="password" className="validate" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }) }} />
                                                        <label for="password">Password</label>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="input-field col s12">
                                                        <a className="btn waves-effect waves-light indigo" onClick={() => this.userLogin()}
                                                            name="login">Login
                                            <i className="material-icons right">https</i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-action">
                                        Doesn't have an Account? <a href="/register">Create here</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}
export default Login;