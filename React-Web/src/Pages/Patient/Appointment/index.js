import React, { Component } from 'react';
import apiCall from './../../../Services/apiV1'


class AppointmentsList extends Component {
    constructor(props) {
        super(props);
        this.state={
            aptList:[],
            pasrList:[]
        }
    }

    async componentDidMount() {
        var payload = {
            username:window.sessionStorage.getItem("username")
        }
        let apiResponse = await apiCall(
            "patient/list-appointment",
            payload
        );
        console.log(apiResponse)
        this.setState({ aptList: apiResponse["aptList"],pastList: apiResponse["pastList"] })
    }

    render() {
        return (
            <div className="row"  style={{ height: "76vh" }}>
                <div className="col s12">
                    <h4>Upcoming Appointment</h4>
                </div>
                <div className="col s12">
                    <table className="striped responsive-table centered">
                        <thead>
                            <tr>
                                <th>Token</th>
                                <th>Date</th>
                                <th>Doctor</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                                this.state.aptList.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{item["token"]}</td>
                                            <td>{item["date"]}</td>
                                            <td>{item["doctor"]}</td>
                                        </tr>
                                    )
                                })
                            }
                            
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
export default AppointmentsList;