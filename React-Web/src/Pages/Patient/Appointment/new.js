import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import apiCall from './../../../Services/apiV1'


class AppointmentNew extends Component {
    constructor(props) {
        super(props);
        this.state={
            docList:[],
            date:"",
            doctor:"",
            history:false
        }
    }

    async componentDidMount() {
        var payload = {

        }
        let apiResponse = await apiCall(
            "admin/list-doctor",
            payload
        );
        console.log(apiResponse)
        this.setState({ docList: apiResponse["list"] })
    }

    async book(){
        var dd = this.state.date.getDate();
        var mm = this.state.date.getMonth() + 1;
  
        var yyyy = this.state.date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var formated_date = dd + '/' + mm + '/' + yyyy;
        if (formated_date == null || formated_date == "") {
            alert("Please select Date");
            return false;
        }else  if (this.state.doctor == null || this.state.doctor == "") {
            alert("Please select Doctor");
            return false;
        }
        var payload = {
            date:formated_date,
            doctor:this.state.doctor,
            history:this.state.history,
            username:window.sessionStorage.getItem("username"),
            publicAddress:window.sessionStorage.getItem("publicAddress")
        }
        console.log(payload)
        let apiResponse = await apiCall(
            "patient/new-appointment",
            payload
        );
        console.log(apiResponse)
        window.location.href="/patient";
    }

    render() {
        return (
            <div className="row"  style={{ height: "76vh" }} >
                <div className="col s6 offset-s3">
                    <h4>New Appointment</h4>
                </div>
                <div className="col s6 offset-s3">
                <div className="row">
                        <div className="input-field col s12">
                            <DatePicker  selected={this.state.date} onChange={(date) => this.setState({date:date})} placeholderText="Appointment Date" dateFormat="dd/MM/yyyy" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                        <select style={{display:"initial"}} value={this.state.doctor} onChange={(e)=>{this.setState({doctor:e.target.value})}}>
                                <option value="" disabled selected>Doctor</option>
                                {
                                this.state.docList.map((item, index) => {
                                    return (
                                        <option value={item["username"]}>{item["name"]}({item["department"]})</option>
                                    )
                                })
                            }
                            </select>
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="input-field col s12">
                            <label>
                                <input type="checkbox" onClick={(e)=>{this.setState({history:e.target.checked})}}/>
                                <span>Allow doctor to access previous records </span>
                            </label>
                        </div>
                    </div>

                    

                    <div className="row">
                        <div className="input-field col s12">
                            <a className="btn waves-effect waves-light" name="login" onClick={()=>this.book()}>Book Now</a>
                        </div>
                    </div>
                </div>
                </div>
        )
    }
}
export default AppointmentNew;