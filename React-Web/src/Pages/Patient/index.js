import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Header from './../../Components/Header'
import Footer from './../../Components/Footer'


import AppointmentList from './Appointment';
import AppointmentNew from './Appointment/new';
import AppointmentPast from './Appointment/past';

class Patient extends Component {
    constructor(props) {
        super(props);
        this.state={
            name : window.sessionStorage.getItem("name"),
            pubAddr :window.sessionStorage.getItem("publicAddress"),
        }
    }

    render() {
        return (
            <div>
            <ul id="slide-out" className="sidenav sidenav-fixed" style={{backgroundColor: "#7986cb"}}>
                <li>
                <a href="#" className="brand-logo" style={{backgroundColor: "#3f51b5",height: "64px",fontSize: "30px",color: "white"}}>HealthChain</a>
                    <div className="user-view">
                        <div className="background" style={{backgroundColor: "#5c6bc0 "}}>
                        </div>
                        <a ><span className="white-text name"><h5>{this.state.name}</h5></span></a>
                            <a ><span className="white-text email" style={{fontSize: "11px"}}>{this.state.pubAddr}</span></a>
                        </div>
                </li>
                <li style={{borderBottom:"1px solid white"}}><a href="/patient/appointment/new">New Appointment</a></li>
                <li style={{borderBottom:"1px solid white"}}><a href="/patient">Appointments</a></li>
                <li style={{borderBottom:"1px solid white"}}><a href="/patient/appointment-past">Past Appointments</a></li>
                

            </ul>
            <div className="wrapper">
            <Header/>
            <Route path={"/patient/"} exact={true} component={AppointmentList} />
            <Route path={"/patient/appointment-past"} exact={true} component={AppointmentPast} />
            <Route path={"/patient/appointment/new"} exact={true} component={AppointmentNew} />
           
            <Footer />

            </div>
        </div>
        )
    }
}
export default Patient;