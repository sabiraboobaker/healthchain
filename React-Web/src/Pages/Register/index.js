import React, { Component } from 'react';
import Header from './../../Components/Header2'
import Footer from './../../Components/Footer2'
import apiCall from './../../Services/apiV1'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            username: '',
            gender: '',
            allergies: '',
            address: '',
            bg: '',
            dob: '',
            publicAddress: '',
            password: ''
        }

    }

    async RegisterPatient() {

        var dd = this.state.dob.getDate();
        var mm = this.state.dob.getMonth() + 1;
  
        var yyyy = this.state.dob.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var formated_date = dd + '/' + mm + '/' + yyyy;
  

        if (this.state.name == null || this.state.name == "") {
            alert("Name can't be blank");
            return false;
        } else if (this.state.username == null || this.state.username == "") {
            alert("Username can't be blank");
            return false;
        } else if (this.state.gender == null || this.state.gender == "") {
            alert("Please select Gender");
            return false;
        }  else if (this.state.address == null || this.state.address == "") {
            alert("Address can't be blank");
            return false;
        } else if (this.state.bg == null || this.state.bg == "") {
            alert("Please select Blood Group");
            return false;
        } else if (formated_date == null || formated_date == "") {
            alert("Please select Date of Birth");
            return false;
        } else if (this.state.publicAddress == null || this.state.publicAddress == "") {
            alert("Public Address can't be blank");
            return false;
        } else if (this.state.password.length < 5) {
            alert("Password must be at least 6 characters long.");
            return false;
        }
        var payload = {
            name:this.state.name,
            username:this.state.username,
            gender:this.state.gender,
            allergies:this.state.allergies,
            address:this.state.address,
            bg:this.state.bg,
            dob:formated_date,
            publicAddress:this.state.publicAddress,
            password:this.state.password,
        }
        console.log(payload)
        let apiResponse = await apiCall(
            "register",
            payload
        );
        console.log(apiResponse)
        window.location.href="/";
    }

    render() {
        return (
            <div>
                <Header />
                <div className="valign-wrapper indigo lighten-4" style={{ height: "90vh" }}>
                    <div className="row" style={{ width: "100vw" }}>
                        <div className="col s12 m6 offset-m3">
                            <div className="card horizontal">
                                <div className="card-stacked">
                                    <div className="card-content">
                                        <span className="card-title">Register</span>
                                        <div className="row">
                                            <div className="col s12">
                                                <div className="row">
                                                    <div className="input-field col s4">
                                                        <input type="text" id="name" className="validate" autofocus value={this.state.name} onChange={(e)=>{this.setState({name:e.target.value})}}/>
                                                        <label for="name">Fullname</label>
                                                    </div>
                                                    <div className="input-field col s4">
                                                        <input type="text" id="username" className="validate" value={this.state.username} onChange={(e)=>{this.setState({username:e.target.value})}}/>
                                                        <label for="username">Username</label>
                                                    </div>
                                                    <div className="input-field col s4">
                                                        <input type="text" id="pubAddress" className="validate" value={this.state.publicAddress} onChange={(e)=>{this.setState({publicAddress:e.target.value})}}/>
                                                        <label for="pubAddress">Public Address</label>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="input-field col s4">
                                                        <select style={{display:"initial"}} id="gender" value={this.state.gender} onChange={(e)=>{this.setState({gender:e.target.value})}}>
                                                            <option value="" disabled selected>Gender</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div className="input-field col s4">
                                                        <select style={{display:"initial"}} id="bg" value={this.state.bg} onChange={(e)=>{this.setState({bg:e.target.value})}} >
                                                            <option value="" disabled selected>Blood Group</option>
                                                            <option value="A+">A+</option>
                                                            <option value="A-">A-</option>
                                                            <option value="B+">B+</option>
                                                            <option value="B-">B-</option>
                                                            <option value="O+">O+</option>
                                                            <option value="O-">O-</option>
                                                            <option value="AB+">AB+</option>
                                                            <option value="AB-">AB-</option>
                                                        </select>
                                                    </div>
                                                    <div className="input-field col s4">
                                                        <DatePicker  selected={this.state.dob} onChange={(date) => this.setState({dob:date})} placeholderText="Date of Birth" dateFormat="dd/MM/yyyy"/>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="input-field col s4">
                                                        <textarea id="address" className="materialize-textarea" value={this.state.address} onChange={(e)=>{this.setState({address:e.target.value})}}></textarea>
                                                        <label for="address">Address</label>
                                                    </div>
                                                    <div className="input-field col s4">
                                                        <textarea id="allergies" className="materialize-textarea" value={this.state.allergies} onChange={(e)=>{this.setState({allergies:e.target.value})}}></textarea>
                                                        <label for="allergies">Allergies</label>
                                                    </div>
                                                    <div className="input-field col s4">
                                                        <input id="password" type="password" className="validate" value={this.state.password} onChange={(e)=>{this.setState({password:e.target.value})}}/>
                                                        <label for="password">Password</label>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="input-field col s12">
                                                        <a className="btn waves-effect waves-light indigo" onClick={()=>this.RegisterPatient()}
                                                            name="register">Register
                                            <i className="material-icons right">https</i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-action">
                                        Already have an Account? <a href="/">Login here</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}
export default Register;