
const url = "http://localhost:3050/"

async function apiCall(action, body) {
  console.log("***API Call***");

  console.log("=>Action: ", action);
  console.log("=>Body: ", body);

  let data = await fetch(url + action, {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body)
  })
  console.log(data)

  let res = await data.json();
  console.log(data.status + " : " + res);
  
  return (res);
}
export default apiCall